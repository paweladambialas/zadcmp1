import express from "express";
import cors from "cors"
import {PhotosController} from "./controllers/PhotosController.js";
const app = express();

app.use(cors());
app.listen(8080, () => {
    console.log("Server running on port 8080");
});
app.get("/photos", (req, res, next)=>{
    const photoControler = new PhotosController();
    photoControler.get(req, res, next);
});

