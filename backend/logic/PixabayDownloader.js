import {PhotoDownloader} from './PhotoDownloader.js';
import {Photo} from "../model/Photo.js";


export class PixabayDownloader extends PhotoDownloader {

    constructor() {
        super();
        this.key = '17538734-356e785b178424adce6cc86eb';
    }

    getBodyFromRequest(body) {
        return JSON.parse(body)['hits'];
    }

    getUrl(q) {
        return `https://pixabay.com/api/?key=${this.key}&q=${q}&per_page=${this.per_page}&page=${this.page}&rating=g&lang=en`
    }

    translateObject(photo) {
        return new Photo(photo.tags, photo.webformatURL, "Pixabay");
    }
}