import https from "https";

export class PhotoDownloader {
    key = '';
    page = 1;
    per_page = 25;

    constructor() {
    }

    search(query) {
        return new Promise((resolve, reject) => {
            https.get(this.getUrl(query), (resp) => {
                let data = '';
                resp.on('data', (chunk) => {
                    data += chunk;
                });
                resp.on('end', () => {
                    const bodyData = this.getBodyFromRequest(data);
                    const results = [];
                    bodyData.forEach(d => {
                        results.push(this.translateObject(d));
                    });
                    resolve(results);
                });
            }).on("error", (err) => {
                reject(err);
            });
        });

    };

    getBodyFromRequest(res) {
        throw new Error('Method not implemented');
    }

    translateObject(d) {
        throw new Error('Method not implemented');
    }

    getUrl(query) {
        throw new Error('Method not implemented');
    }
}