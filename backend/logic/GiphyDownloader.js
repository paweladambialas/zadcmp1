import {PhotoDownloader} from './PhotoDownloader.js';
import {Photo} from "../model/Photo.js";


export class GiphyDownloader extends PhotoDownloader {

    constructor() {
        super();
        this.key = 'ysLvCPYyHCHxB94cXNGiKnnGcpxFcs2c';
    }

    getBodyFromRequest(body) {
        return JSON.parse(body)['data'];
    }
    getUrl(q) {
        return `https://api.giphy.com/v1/gifs/search?api_key=${this.key}&q=${q}&limit=${this.per_page}&offset=${this.getOffset()}&rating=g&lang=en`
    }
    getOffset() {
        return (this.page -1)*this.per_page;
    }

    translateObject(photo) {
        return new Photo(photo.title, photo.images.original.url, "Giphy");
    }
}