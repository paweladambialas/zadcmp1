import {PixabayDownloader} from '../logic/PixabayDownloader.js';
import {GiphyDownloader} from '../logic/GiphyDownloader.js';


export class PhotosController {
    things = ['Rock', 'Paper', 'COVID', 'mom', 'luck', 'happy'];
    get(req, res, next) {
        console.log(`Phrase ${req.query.q}`);
        const downloaders = [new GiphyDownloader(), new PixabayDownloader() /* TODO MORE*/];
        const searchPhrase = this.getPhrase(req.query.q);
        let result = [];
        let promises = [];

        downloaders.forEach(function (downloader) {
            promises.push(downloader.search(searchPhrase));
        }.bind(this));

        Promise.all(promises).then(response => {
            response.forEach(data => {
                result = result.concat(data);
            });
            res.json(result);
        }).catch(error => {
            console.error(error);
            res.sendStatus(500);
        });
    }

    getPhrase(searchPhrase) {
        if (searchPhrase.length === 0) {
            searchPhrase = this.things[Math.floor(Math.random() * this.things.length)];
        }
        return searchPhrase;
    }
}