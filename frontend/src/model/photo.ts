export interface Photo {
    name: string;
    url: string;
    source: string;
}