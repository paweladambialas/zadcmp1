import React from 'react';

import {Photo} from "./model/photo";

export function PhotoList(photos: Photo[]) {
    return photos.map((photo: Photo) => (
        <div className="col-sm-6 col-md-3">
            <div className="card m-1 w-100">
                <img src={photo.url} alt={photo.name} className="img-responsible"/>
                <div className="card-body">
                    <p className="card-text">
                        {photo.name} <br/>
                        Source: {photo.source}
                    </p>
                </div>
            </div>
        </div>
    ));
};
