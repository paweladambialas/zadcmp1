import React, {useState, useEffect, SyntheticEvent} from 'react';
import './App.css';
import {Photo} from "./model/photo";
import {Config} from "./Config";
import {PhotoList} from "./PhotoList";

export default function App() {
    const [photos, setPhotos] = useState<Photo[]>([]);
    const [query, setQuery] = useState("");
    useEffect(() => {
        downloadPhotos();
    }, []);

    function downloadPhotos() {
        fetch(Config.HOST + "/photos?q=" + query)
            .then(res => res.json())
            .then(
                (result) => {
                    setPhotos(result);
                },
                (error) => {
                    console.error(error);
                }
            )
    }

    function onFormSubmit(e: SyntheticEvent) {
        e.preventDefault();
        console.log(e);
        downloadPhotos();
    }

    return (
        <div className="container">
            <form onSubmit={onFormSubmit}>
                <input name="query" onChange={e => setQuery(e.target.value)}/>
                <button type="submit">Search</button>
            </form>
            <div className="row">
                {
                    PhotoList(photos)
                }
            </div>
        </div>
    );
}

