## Requirements 
Nodejs 13+ 
NPM 6+

## Run backend 
    cd backend
    npm install 
    node main.js
## Run frontend 
    cd frontend
    npm install
    npm start 